import express from 'express'
import { knex } from './db'

let router = express.Router()

export default router

router.get('/events', async (req, res) => {
  let rows = await knex.select('*').from('event_type')
  res.json(rows)
})

router.post('/events/:event_type', async (req, res) => {
  let query = knex
    .insert({
      event_type_id: knex
        .select('id')
        .from('event_type')
        .where({ event_type: req.params.event_type }),
      points: req.body.points,
    })
    .into('event_record')
  let sql = query.toSQL()
  console.log(sql)
  let result = await query
  res.json(result)
})
