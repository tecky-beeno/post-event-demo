import { Knex } from "knex";


export async function up(knex: Knex): Promise<void> {
    await knex.schema.createTable('event_type',table=>{
        table.increments()
        table.string('event_type')
    })
}


export async function down(knex: Knex): Promise<void> {
    await knex.schema.dropTable('event_type')
}

