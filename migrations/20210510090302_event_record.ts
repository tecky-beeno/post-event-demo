import { Knex } from 'knex'

export async function up(knex: Knex): Promise<void> {
  await knex.schema.createTable('event_record', table => {
    table.increments()
    table.integer('points')
    table.integer('event_type_id').references('event_type.id')
  })
}

export async function down(knex: Knex): Promise<void> {
  await knex.schema.dropTable('event_record')
}
