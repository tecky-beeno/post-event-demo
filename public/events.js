console.log('events.js')

let main = document.querySelector('main')

async function loadData(){
  main.innerHTML = '<p>Loading...</p>'
  let res = await fetch('/events')
  let event_types = await res.json()
  console.log('data:', event_types)
  main.innerHTML =`
  <div class="event-type-list">
  ${event_types.map(event_type=>`
    <div class="event-type">
      <a>
        ${event_type.event_type}
      </a>
    </div>
  `).join('')}
  </div>
  `
}

loadData()
