console.log('reaction.js')

let main = document.querySelector('main')

let points = 0
setInterval(() => {
  points++
}, 300)

async function loadData() {
  main.innerHTML = '<p>Loading...</p>'
  let res = await fetch('/reaction')
  let data = await res.json()
  console.log('data:', data)
  if (typeof data === 'object') {
    console.table(data)
  }
  main.innerHTML = '<p>Coming Soon...</p>'
}

loadData()

async function submitPoints() {
  let res = await fetch('/events/reaction', {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
    },
    body: JSON.stringify({ points }),
  })
  let result = await res.json()
  console.log('result:', result)
}
