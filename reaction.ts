import express from 'express'

let router = express.Router()

export default router

router.get('/reaction', (req, res) => {
  res.json('reaction data')
})
