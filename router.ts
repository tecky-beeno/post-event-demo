import express from 'express'

export let router = express.Router()

router.get('/session', (req, res) => {
  res.json(req.session)
})

import eventsRouter from './events'
router.use(eventsRouter)

import reactionRouter from './reaction'
router.use(reactionRouter)

import squatRouter from './squat'
router.use(squatRouter)
