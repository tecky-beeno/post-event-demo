import { Knex } from "knex";

export async function seed(knex: Knex): Promise<void> {
    // Deletes ALL existing entries
    await knex("event_type").del();

    // Inserts seed entries
    await knex("event_type").insert([
        { event_type:'reaction' },
        { event_type:'squat' },
        { event_type:'quickness' },
    ]);
};
